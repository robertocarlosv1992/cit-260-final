package finalProject;

public class Checking extends Accounts{

    //Parameter constructor.
    public Checking(double balance){
        setBalance(balance);
    }

    //Overrides withdrawal() from Accounts
    public void withdrawal(){

        System.out.println("This operation will cost you $1.00");
        System.out.print("How much do you want to withdraw: ");
        withdraw = input.nextInt();
        transactions = getBalance();
        setBalance(transactions - withdraw - 1);
        System.out.println("--------------------------------");
        System.out.println("You withdrew: $" + withdraw);
        System.out.println("Your current balance is: $" + getBalance());
        System.out.println("--------------------------------");
    }

    //We will use this method when we add the write and read functionality of our program.
    public String toString() {
        return "Checking: balance: $" + balance;
    }
}
