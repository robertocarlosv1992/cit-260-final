package finalProject;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //main() creates an array of accounts. It contains the main menu that sends the user to the different accounts.
        System.out.println("-----------------------------------");
        System.out.println("**** Welcome to the ATM system ****");
        System.out.println("-----------------------------------");

        //Array of accounts object.
        ArrayList<Accounts> accountsList = new ArrayList<>();

        //Adds a checking and a savings accounts with hardcoded parameters.
        accountsList.add(new Checking(500));
        accountsList.add(new Savings(2000));

        Scanner input = new Scanner(System.in);

        //Handles the selection between the savings and checking accounts.
        int flag = 0;
        int selection;
        do {
            do{
                //Prints main menu.
                System.out.println("Please select an account:");
                System.out.println("    1. Checking");
                System.out.println("    2. Savings");
                System.out.println("    3. Exit");
                System.out.print("> ");

                selection = input.nextInt();
                //Validates input.
                if (selection >= 1 && selection <= 3){
                    flag = 1;
                } else {
                    System.out.println("--------------------------------");
                    System.out.println("Invalid input, please try again.");
                    System.out.println("--------------------------------");
                }
            }while (flag == 0);

            if (selection == 1){  //Calls operations() from the first item in the array (Checking Account).
                System.out.println("****   Checking Account   ****");
                accountsList.get(0).operations();
            } else if (selection == 2) {  //Calls operations() from the second item in the array (Savings Account).
                System.out.println("****    Savings Account    ****");
                accountsList.get(1).operations();
            } else if (selection == 3) {  //Exits the program.
                System.out.println("---------------------------------");
                System.out.println("Thank you. Please come back soon.");
                System.out.println("---------------------------------");
                flag = 2;
            }
        }while (flag != 2); //Exits when flag == 2

    }
}
