package finalProject;

public class Savings extends Accounts{

    //Counts how many times the user withdraws money.
    int withdrawTimes = 0;

    //Parameter constructor.
    public Savings(double balance){
        setBalance(balance);
    }

    //Overrides withdrawal() from Accounts
    public void withdrawal(){

        if (withdrawTimes < 4){
            System.out.println("Savings accounts are restricted to 3 withdrawals per session.");
            System.out.println("You can withdraw money " + (3 - withdrawTimes) + " more time(s).");
            //Calls withdrawal() from accounts
            super.withdrawal();
            withdrawTimes++;
        }
        else
            System.out.println("Error. You can't withdraw money from savings.");
    }

    //Overrides deposit() from Accounts
    public void deposit(){
        System.out.println("Savings accounts earn a 1% interest every time you make a deposit.");
        System.out.print("How much do you want to deposit: ");
        deposit = input.nextInt();
        transactions = getBalance();
        setBalance((transactions + deposit) * 1.01);
        System.out.println("--------------------------------");
        System.out.println("You deposited: $" + deposit);
        System.out.println("Your current balance is: $" + getBalance());
        System.out.println("--------------------------------");
    }

    //We will use this method when we add the write and read functionality of our program.
    public String toString() {
        return "Checking: balance: $" + balance;
    }
}
