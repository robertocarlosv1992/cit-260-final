package finalProject;

import java.util.Scanner;

public class Accounts {

    protected double transactions, withdraw, deposit, balance;

    Scanner input = new Scanner(System.in);

    //Handles operation selection from accounts.
    public void operations() {
        int flag = 0;
        int selection;
        do {
            do{
                System.out.println("Please select an option:");
                System.out.println("    1. Check balance");
                System.out.println("    2. Withdrawal");
                System.out.println("    3. Deposit");
                System.out.println("    4. Exit");
                System.out.print("> ");

                //Input validation.
                selection = input.nextInt();
                if (selection >= 1 && selection <= 4){
                    flag = 1;
                } else {
                    System.out.println("--------------------------------");
                    System.out.println("Invalid input, please try again.");
                    System.out.println("--------------------------------");
                }
            }while (flag == 0);

            //Calls method according to the selection.
            if (selection == 1){
                checkBalance();
            } else if (selection == 2) {
                withdrawal();
            } else if (selection == 3) {
                deposit();
            } else if (selection == 4) {
                System.out.println("--------------------------------");
                System.out.println("Going back to main menu...");
                System.out.println("--------------------------------");
                flag = 2;
            }
        }while (flag !=2);
    }

    //This is the same for all accounts. That's why it is not overridden.
    public void checkBalance(){
        System.out.println("--------------------------------");
        System.out.println("Your current balance is: $" + getBalance());
        System.out.println("--------------------------------");
    }

    //Generic withdrawal
    public void withdrawal(){
        System.out.print("How much do you want to withdraw: ");
        withdraw = input.nextInt();

        //Checks the user has enough money for the transaction.
        if(withdraw <= getBalance()){
            transactions = getBalance();
            setBalance(transactions - withdraw);
            System.out.println("--------------------------------");
            System.out.println("You withdrew: $" + withdraw);
            System.out.println("Your current balance is: $" + getBalance());
        } else {
            System.out.println("--------------------------------");
            System.out.println("Error. Insufficient founds.");
        }
        System.out.println("--------------------------------");
    }

    //Generic deposit
    public void deposit(){
        System.out.print("How much do you want to deposit: ");
        deposit = input.nextInt();
        transactions = getBalance();
        setBalance(transactions + deposit);
        System.out.println("--------------------------------");
        System.out.println("You deposited: $" + deposit);
        System.out.println("Your current balance is: $" + getBalance());
        System.out.println("--------------------------------");
    }

    //Getter and Setter
    public double getBalance(){
        return balance;
    }
    public void setBalance(double balance){
        this.balance = balance;
    }

    //toString. We will use this method when we add the write and read functionality of our program.
    @Override
    public String toString() {
        return "Accounts: balance: $" + balance;
    }
}
